//
//  ChooseVC.swift
//  lendy_ios
//
//  Created by Axel Proust on 09/06/2018.
//  Copyright © 2018 lendy.ios. All rights reserved.
//

import UIKit

class ChooseVC : UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var buttonGiveCar: UIButton!
    @IBOutlet weak var buttonSearchCar: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func giveCarAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToTuto", sender: nil)
    }
    
    @IBAction func searchCarAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToTuto", sender: nil)
    }
    
    private func initUI() {
        contentView.dropShadow()
        contentView.layer.cornerRadius = 5
        buttonGiveCar.layer.cornerRadius = 5
        buttonGiveCar.dropShadow()
        buttonGiveCar.backgroundColor = UIColor.mainLendyBlueColor
        buttonSearchCar.layer.cornerRadius = 5
        buttonSearchCar.dropShadow()
        buttonSearchCar.backgroundColor = UIColor.mainLendyBlueColor
    }
}
