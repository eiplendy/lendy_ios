//
//  LoginVC.swift
//  lendy_ios
//
//  Created by Axel Proust on 09/06/2018.
//  Copyright © 2018 lendy.ios. All rights reserved.
//

import UIKit

class LoginVC : UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var buttonLogin: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUi()
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "GoToChoose", sender: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    private func initUi() {
        contentView.dropShadow()
        contentView.layer.cornerRadius = 5
        buttonLogin.layer.cornerRadius = 5
        buttonLogin.backgroundColor = UIColor.mainLendyBlueColor
        buttonLogin.dropShadow()
    }
}
