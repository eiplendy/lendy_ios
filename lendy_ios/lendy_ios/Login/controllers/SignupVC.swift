//
//  SignupVC.swift
//  lendy_ios
//
//  Created by Axel Proust on 09/06/2018.
//  Copyright © 2018 lendy.ios. All rights reserved.
//

import UIKit

class SignupVC : UIViewController {
    
    @IBOutlet weak var buttonRegister: UIButton!
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearNavigationBar(navigationController: self.navigationController)
        initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    
    @IBAction func registerAction(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    private func initUI() {
        contentView.dropShadow()
        contentView.layer.cornerRadius = 5
        buttonRegister.layer.cornerRadius = 5
        buttonRegister.dropShadow()
        buttonRegister.backgroundColor = UIColor.mainLendyBlueColor
    }
}
