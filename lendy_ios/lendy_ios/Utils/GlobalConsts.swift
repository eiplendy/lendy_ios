//
//  GlobalConsts.swift
//  lendy_ios
//
//  Created by Axel Proust on 10/06/2018.
//  Copyright © 2018 lendy.ios. All rights reserved.
//

import Foundation
import UIKit

struct GlobalConst {
    
}


extension UIColor {
    static let mainLendyBlueColor = #colorLiteral(red: 0.1921568627, green: 0.3254901961, blue: 0.8235294118, alpha: 1)
    
}

extension NSNotification.Name {
    static let pagerChangeValue = Notification.Name("PAGER_CHANGE_VALUE")
}


