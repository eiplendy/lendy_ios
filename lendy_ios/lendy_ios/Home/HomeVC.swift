//
//  HomeVC.swift
//  lendy_ios
//
//  Created by Axel Proust on 09/06/2018.
//  Copyright © 2018 lendy.ios. All rights reserved.
//

import UIKit

class HomeVC : UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contentView: UIView!
    var numberOfUsers = 10
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
     
        self.tableView.layer.cornerRadius = 5
        self.contentView.layer.cornerRadius = 5
        self.contentView.dropShadow()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func deleteUser(_ sender: Any) {
        guard numberOfUsers > 0 else { return }
        numberOfUsers -= 1
        tableView.reloadData()
    }
    
}

extension HomeVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfUsers
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: "cell")!
    }
    
}
