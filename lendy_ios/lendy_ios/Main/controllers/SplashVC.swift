//
//  SplashVC.swift
//  lendy_ios
//
//  Created by Axel Proust on 09/06/2018.
//  Copyright © 2018 lendy.ios. All rights reserved.
//

import UIKit

class SplashVC : UIViewController {
    
    override func viewDidLoad() {
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        self.navigationController?.isNavigationBarHidden = true
         self.performSegue(withIdentifier: "goToLogin", sender: nil)
    }
    
}
