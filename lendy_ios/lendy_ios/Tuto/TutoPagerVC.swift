//
//  TutoPagerVC.swift
//  lendy_ios
//
//  Created by Axel Proust on 09/06/2018.
//  Copyright © 2018 lendy.ios. All rights reserved.
//

import UIKit

class TutoPagerVC: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    var hide = false
    let pageControl = UIPageControl()
    var pages = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.dataSource = self
        
        let firstController = storyboard?.instantiateViewController(withIdentifier: "FirstVC")
        let secondController = storyboard?.instantiateViewController(withIdentifier: "SecondVC")
         let thirdController = storyboard?.instantiateViewController(withIdentifier: "ThirdVC")
       
        
        self.pages.append(firstController!)
        self.pages.append(secondController!)
        self.pages.append(thirdController!)
        setViewControllers([firstController!], direction: .forward, animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard pages.count > previousIndex else {
            return nil
        }
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        guard nextIndex < pages.count else {
            return nil
        }
        guard pages.count > nextIndex else {
            return nil
        }
        return pages[nextIndex]
    }
    
    private func VCInstance(name: String) -> UIViewController {
        return UIStoryboard(name: "Tuto", bundle: nil).instantiateViewController(withIdentifier: name)
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name.pagerChangeValue, object: nil, userInfo: ["index": pages.index(of: (pageViewController.viewControllers?.first)!)!])
    }
}
