//
//  TutoVC.swift
//  lendy_ios
//
//  Created by Axel Proust on 09/06/2018.
//  Copyright © 2018 lendy.ios. All rights reserved.
//

import UIKit

class TutoVC: UIViewController {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var buttonUnderstood: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.numberOfPages = 3
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        initUi()
        initObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clearNavigationBar(navigationController: self.navigationController)
        
    }
    
    private func initObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(changeIndex), name: NSNotification.Name.pagerChangeValue, object: nil)
    }
    
    private func initUi() {
        contentView.dropShadow()
        contentView.layer.cornerRadius = 5
        buttonUnderstood.layer.cornerRadius = 5
        buttonUnderstood.backgroundColor = UIColor.mainLendyBlueColor
        buttonUnderstood.dropShadow()
    }
}

@objc extension TutoVC {
    func changeIndex(notification: NSNotification) {
        guard let index = notification.userInfo!["index"] as? Int else {return}
        self.pageControl.currentPage = index
    }
}
